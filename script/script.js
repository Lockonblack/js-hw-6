/////--------------Теоретичні питання-------
///1 Для пошуку спеціальних символів
// [ \ ^ $ . | ? * + ( ), нам потрібно додати перед ними \ (“екранувати їх”).

///2  Спочатку ми пишемо function — це ключове слово (keyword), яке дає зрозуміти комп’ютеру,
// що далі буде оголошення функції. Потім — назву функції і список її параметрів в дужках
// (розділені комою). Якщо параметрів немає, ми залишаємо пусті дужки. І нарешті, код функції,
// який також називають тілом функції між фігурними дужками.


///3 Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції
// піднімаються вгору по своїй області видимості перед виконанням коду. Однією з переваг підйому
// є те, що він дозволяє нам використовувати функції перед їх оголошенням у коді.


/////----------------------------------HomeWorks------------------------///////////////////

function createNewUser() {
    let firstName = prompt("Введіть своє ім'я:")
    let lastName = prompt("Введіть прізвище:")
    let date = prompt ("Введіть дату народження. dd.mm.yyyy");
    //let birthday = prompt ("Введіть дату народження. dd.mm.yyyy");
    console.log(date);
    let date1 = date.split('');
    console.log(date1);
    let date2 = date1.map(elem => {
        if(elem === '.') {
            elem = '-';
        }
        return elem;
    })
    let date3 = (date2.join(''));
    console.log(date3);
    let date4 = (date3.replace(/^(\d+)-(\d+)-(\d+)$/, `$3-$2-$1`));
    // console.log(date4.replace(/^(\d+)-(\d+)-(\d+)$/, `$3-$2-$1`));
    let birthday = '' + date4;
    console.log(birthday)

    const newUser = {
        firstName_: firstName,
        lastName_: lastName,
        birthday : birthday,
        getLogin: function () {
            return(this.firstName_[0] + this.lastName_).toLowerCase();
        },
        getAge : function (){
            const today = new Date();
            const birthDate = new Date(birthday);
            let age = today.getFullYear() - birthDate.getFullYear();
            const monthAge = today.getMonth() - birthDate.getMonth();
            if (monthAge < 0 || (monthAge === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        getPassword: function () {
            const firstInitial = this.firstName_[0].toUpperCase();
            const lastNameLower = this.lastName_.toLowerCase();
            const userAge = this.birthday.split('-')[0];
            return firstInitial + lastNameLower + userAge;
        },


        // setFirstName: function (newFirstName){
        //     if (typeof newFirstName === 'string'){
        //         this.firstName_ = newFirstName;
        //     }
        //         else {
        //             console.log("Невалідне значення імені")
        //     }
        // },
        // setLastName: function (newLastName) {
        //     if (typeof newLastName === 'string') {
        //         this.lastName_ = newLastName;
        //     } else {
        //         console.log("Невалідне значення для прізвища.")
        //     }
        // }
    };
    Object.defineProperty(newUser, "firstName", {writable: false});
    Object.defineProperty( newUser, "lastName", {writable: false});
    return newUser;
}

let user = createNewUser();
console.log("Логін користувача :",user.getLogin());
const age = user.getAge();
console.log("Вік користувача:", age);
const password = user.getPassword();
console.log("Пароль:", password);
// user.setFirstName("Kolya");
// user.setLastName("Levine");
// console.log(user.getLogin());